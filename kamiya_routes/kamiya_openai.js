require('dotenv').config();

const express = require('express');
const bodyparser = require('body-parser');
const request = require('then-request');

const { Configuration, OpenAIApi } = require('openai');
const kv = require("../kamiya_modules/key-value");
const fs = require("fs");
const imageUpload = require("../kamiya_modules/image_upload");
const cache = require('../kamiya_modules/session_cache');

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

process.env.TZ = 'Asia/Shanghai';

const app = express.Router();

app.use(bodyparser.json({ limit:'1000mb'}));

const D = new kv('./data/data.json');

const S = new kv('./data/pass.json');

const C = new kv('./data/openai_conversation.json');

function check_pass(pass) {
    if(cache.getCheck(pass)) return true;
    else {
        if(!S.get(`${pass}.logout`) && S.get(`${pass}.token`) && (Date.parse(new Date()) - S.get(`${pass}.time`)) < 604800000) {
            cache.setCheck(pass);
            return true;
        }
        return false;
    }
};

function uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
}

function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),
        "m+": (date.getMonth() + 1).toString(),
        "d+": date.getDate().toString(),
        "H+": date.getHours().toString(),
        "M+": date.getMinutes().toString(),
        "S+": date.getSeconds().toString()
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
        };
    };
    return fmt;
}

function get_token(pass) {
    if(cache.getSession(pass)) {
        return cache.getSession(pass);
    }
    else {
        const token = S.get(`${pass}.token`);
        cache.setSession(pass,token);
        return token;
    }
};
function get_left(token) {return D.get(`${token}.left`);};

function cut_prompt(prompt, context, conversation_id, max_length) {
    return new Promise((resolve) => {
        prompt = context + 'Human:' + prompt + '\n AI:';
        if(prompt.length <= max_length) r = prompt;
        r = prompt.substring(prompt.length - max_length,prompt.length);
        resolve(r);
    });
}

/*
function cut_prompt(prompt, context, conversation_id, max_length) {
    const p = new Promise((resolve) => {
        let r = ''
        prompt = context + 'Human:' + prompt + '\n AI:';
        request('POST',process.env.PROMPT_SERVER,{json: {
                prompt: prompt,
                history_str: prompt,
                token_limit: 1500,
                extra_token: 350
            }}).getBody('utf8').then(JSON.parse).done(function(R) {
            console.log({
                id: conversation_id,
                status: 'nlp http code 200',
                response: R
            });
            if(R.code != 200) {
                prompt = context + 'Human:' + prompt + '\n AI:';
                if(prompt.length <= max_length) r = prompt;
                r = prompt.substring(prompt.length - max_length,prompt.length);
                resolve(r);
            }
            for (let i in R.data) {
                r += '\n' + R.data[i];
            }
            resolve(r);
        },(e) =>{
            console.log({
                id: conversation_id,
                status: 'nlp http error'
            },e);
            prompt = context + 'Human:' + prompt + '\n AI:';
            if(prompt.length <= max_length) r = prompt;
            r = prompt.substring(prompt.length - max_length,prompt.length);
            resolve(r);
        });
    });
    return p;
}
*/

app.post('/api/openai/conversation',async (req,res) => {
    //console.log(req.body);

    const pass = req.body.pass;

    if(!pass || !check_pass(pass)) {res.send({success: false});return;};
    const token = get_token(pass);
    if(!(get_left(token) > 0)) {res.send({success: false,message: '剩余魔晶点数不足'});return;};

    const action = req.body.action;
    let conversation_id = req.body.conversation_id;

    let head;

    if(conversation_id) {
        let ctx = C.get(conversation_id);
        head = ctx;
    }
    else {
        conversation_id = uuid();
        head = req.body.head;
        if(!head) head = 'The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\nHuman: 你好，你能帮助我做什么？\\nAI: 我是由OpenAI创造的AI，有什么可以帮到你的吗？\n'
    }

    let prompt = req.body.prompt;
    let ctx = head + 'Human:' + prompt + '\n AI:';
    cut_prompt(prompt,head,conversation_id,1500).then((prompt) => {
        let o = JSON.parse(fs.readFileSync('./logs/chatdemo.json'));
        if(o[dateFormat("YYYY-mm-dd",new Date())]) o[dateFormat("YYYY-mm-dd",new Date())] += 1;
        else o[dateFormat("YYYY-mm-dd",new Date())] = 1;
        fs.writeFileSync('./logs/chatdemo.json',JSON.stringify(o,0,2));

        openai.createCompletion({
            model: 'text-davinci-003',
            prompt: prompt,
            temperature: 0.9,
            top_p: 1,
            frequency_penalty: 0,
            presence_penalty: 0.6,
            max_tokens: 350,
            stop: [
                'Human:',
                'AI:'
            ]
        }).then((R) => {
            D.put(`${token}.left`,(get_left(token) - 0.1).toFixed(1) * 1);
            const result = R.data.choices[0].text;
            C.put(conversation_id,ctx + result + '\n');
            res.send({
                success: true,
                conversation_id: conversation_id,
                result: result
            });
            console.log({
                success: true,
                conversation_body: req.body,
                result: result
            });
        },(e) => {
            console.log(e.response.data.error);
            res.send({
                success: false,
                conversation_id: conversation_id,
                message: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversation_id
            });
            console.log({
                success: false,
                conversation_body: req.body,
                message: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversation_id
            });
        });
    });
});

app.get('/api/openai/get_context',(req,res) => {
    if(req.query.id){
        res.setHeader('Content-Type','application/octet-stream');
        res.setHeader('Content-Disposition','attachment; filename=kamiya_chatgpt_demo_context_' + req.query.id + '_' + dateFormat('YYYY-mm-dd_HH-MM-SS',new Date()) + '.json');
        res.send({
            success: true,
            context: C.get(req.query.id)
        });
    }
    else res.send({success: false});
});

app.post('/api/generate-voice',(req,res) => {
    const pass = req.body.pass;

    const u = uuid();
    console.log({
        req: req.body,
        uuid: u
    })

    if(!pass || !check_pass(pass)) {res.send({success: false});return;};
    const token = get_token(pass);
    if(!(get_left(token) > 0)) {res.send({success: false,message: '剩余魔晶点数不足'});return;};

    let o = JSON.parse(fs.readFileSync('./logs/tts.json'));
    if(o[dateFormat("YYYY-mm-dd",new Date())]) o[dateFormat("YYYY-mm-dd",new Date())] += 1;
    else o[dateFormat("YYYY-mm-dd",new Date())] = 1;
    fs.writeFileSync('./logs/tts.json',JSON.stringify(o,0,2));

    let cost = 0.5;

    if(req.body.prompt.length > 300) {
        res.send({
            success: false,
            message: '请求的文字过长，将此ID上报以快速定位此次错误' + u
        });
        return;
    }

    const backendL = JSON.parse(fs.readFileSync('./data/backend.json'));

    switch(req.body.type) {
        case 'moegoe': {
            const node = backendL.moe[Math.floor(Math.random() * backendL.moe.length)];
            request('POST',node.url,{json: {
                    model_name: node.model,
                    task_id: Math.floor(Math.random() * 1024),
                    text: '[ZH]' + req.body.prompt + '[ZH]',
                    speaker_id: 0
                }}).getBody('utf-8').then(JSON.parse).done((R) => {
                    if(R.code == 200) {
                        res.send({
                            success: true,
                            audio: R.audio,
                            type: 'moegoe',
                            speaker: R.speaker,
                            speaker_pic: node.pic_url
                        });
                        D.put(`${token}.left`,(get_left(token) - cost).toFixed(1) * 1);
                    }
                    else {
                        res.send({
                            success: false,
                            message: R.msg + '，音频合成后端错误，将此ID上报以快速定位此次错误' + u
                        });
                    }
            },(e) => {
                    console.log(e);
                    res.send({
                        success: false,
                        message: '音频合成后端错误，将此ID上报以快速定位此次错误' + u
                    });
            });
            break;
        }
        case 'azure': {
            break;
        }
        default: {
            break;
        }
    }

});

module.exports = app;